class arbol2907
{
    constructor()
    {
        this.nodoPadre = "";
        this.nodos = [];
    }

    NodoPadre(padre, nivel, posicion, valor, nombre)
    {
        let noDo = new Nodo2907(padre, nivel, posicion, valor, nombre);
        this.nodos.push(noDo);
        return noDo;
    }

    Nodo(padre, nivel,posicion, Valor, nombre)
    {
        var nodo = new Nodo2207(padre,nivel, posicion, Valor, nombre);
        this.nodos.push(nodo);
        return nodo;
    }

    buscarNodoPadres(padre)
    {
        let nodosEncontrados = [];
        for (let x = 0; x < this.nodos; x++)
        {
            if(this.nodos[x].padre == padre)
            {
                nodosEncontrados.push(this.nodos[x]);
            }
        }
        return nodosEncontrados;
    }

    agregarNodo(Valor, nombre)
    {
        let noDo = new Nodo2907(null, null, null, Valor, nombre);
        if(this.nodos.length == 1)
        {
            noDo.padre = this.nodoPadre;
            noDo.nivel = this.nodoPadre.nivel + 1;
            if(noDo.Valor < this.nodoPadre.Valor)
            {
                noDo.posicion = 'hIzq';
            }
            else
            {
                noDo.posicion = 'hDer';
            }
            this.nodos.push(noDo);
        }
    }
}