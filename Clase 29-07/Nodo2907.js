class Nodo2907
{
    constructor(Padre = null, Nivel= null, posicion = null, Valor, nombre) {
        this.padre = Padre;
        this.nivel = Nivel;
        this.posicion = posicion;
        this.Valor = Valor;
        this.nombre = nombre;
        this.caracteristica = {
            "Persona 1": {
                "Nombre" : "David",
                "Apellidos": "Vite Bautista",
                "Gustos": ["Musica", "Peliculas"],
                "Color" : "Morado"
            },
            "Persona 2": {
                "Nombre" : "David x2",
                "Apellidos": "Vite Bautista x2",
            },
            "Saludo" : "Hola Mundo",
        };
    }

    hIzq(Padre, Nivel, posicion, Valor, nombre)
    {
        var nodo = new Nodo2207(Padre, Nivel, posicion, Valor, nombre);
        return nodo;
    }

    hDer(Padre, Nivel, posicion, Valor, nombre)
    {
        var nodo = new Nodo2207(Padre, Nivel, posicion, Valor, nombre);
        return nodo;
    }
}