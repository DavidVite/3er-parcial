class ArbolE
{
    constructor()
    {
        this.nodoPadre = "";
        this.nodos = [];
    }

    NodoPadre(padre, nivel, posicion, valor, nombre)
    {
        let noDo = new NodoE(padre, nivel, posicion, valor, nombre);
        this.nodos.push(noDo);
        return noDo;
    }

    Nodo(padre, nivel,posicion, Valor, nombre)
    {
        var nodo = new NodoE(padre,nivel, posicion, Valor, nombre);
        this.nodos.push(nodo);
        return nodo;
    }


    agreNodon1(Valor, nombre)
    {
        let noDo = new NodoE(null, null, null, Valor, nombre);
        if(this.nodos.length == this.nodos.length)
        {
            noDo.padre = this.nodoPadre;
            noDo.nivel = this.nodoPadre.nivel + 1;
            if(noDo.Valor < this.nodoPadre.Valor)
            {
                noDo.posicion = 'NodoIzquierdo';
            }
            else
            {
                noDo.posicion = 'NodoDerecho';
            }
            this.nodos.push(noDo);
        }

    }

    agreNodon2(Valor, nombre)
    {
        let noDo = new NodoE(null, null, null, Valor, nombre);
        if(this.nodos.length == this.nodos.length)
        {
            noDo.padre = this.nodos [1];
            noDo.nivel = this.nodoPadre.nivel + 2;
            if(noDo.Valor < this.nodos[1].Valor)
            {
                noDo.posicion = 'NodoIzquierdo';
            }
            else
            {
                noDo.posicion = 'NodoDerecho';
            }
            this.nodos.push(noDo);
        }
    }
}