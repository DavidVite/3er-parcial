class Arbol20
{
    constructor()
    {
        this.nodoPadre = "";
        //this.nivel = 2;
        //this.Busquedaelementos;
        //this.Buscarnodos = [];
        //this.Caminonodo = '';
        //this.Sumacaminos = 0;
    }

    NodoPadre(padre, nivel, posicion, valor, nombre)
    {
        let nodo = new Nodo20(padre,nivel,posicion, valor, nombre);
        return nodo;
    }

    agregarNodo(nodoPadre,posicion,nombre, Valor)
    {
        var nodo = new Nodo20(nodoPadre,posicion,nombre, Valor);
        return nodo;
    }

    verificarNivelHijos(nodo)
    {
        if(nodo.nivel == this.nivel)
            this.Buscarnodos.push(nodo.Nombre);

        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.Buscarnodos;
    }

    buscarValor(Buscarelemento, nodo)
    {
        if(nodo.Valor == Buscarelemento)
            this.Busquedaelementos = nodo;

        if(nodo.hasOwnProperty('hI'))
            this.buscarValor(Buscarelemento, nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.buscarValor(Buscarelemento, nodo.hD);

        return this.Busquedaelementos;
    }

    buscarCaminoNodo(nodo)
    {
        if(nodo.padre != null)
        {
            this.Caminonodo = this.Caminonodo+' '+nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre)
        }
        return this.Busquedaelementos.nombre+'  '+this.Caminonodo;
    }

    sumarCaminoNodo(nodo){
        if(nodo.padre != null)
        {
            this.Sumacaminos = this.Sumacaminos+nodo.padre.Valor;
            this.sumarCaminoNodo(nodo.padre);
        }
        return this.Busquedaelementos.Valor + this.Sumacaminos;
    }
}