//Ejemplos
var persona = [];
persona.push("David");
persona.push("Vite");
persona.push("19");

var gustosDavid = [];
gustosDavid.push("Musica");
gustosDavid.push("Ver Peliculas");


//Otras Formas
//Forma 1
var Familia1 = {
    "Persona 1": "David",
    "Apellidos" : "Vite Bautista"
};
//console.log(Familia1);


//Forma 2
var Familia2 = {
    "Persona 1": {
        "Nombre" : "David",
        "Apellidos": "Vite Bautista",
        "Gustos": ["Musica", "Peliculas"],
        "Color" : "Morado"
    },
    "Persona 2": {
        "Nombre" : "David x2",
        "Apellidos": "Vite Bautista x2",
    },
    "Saludo" : "Hola Mundo",
};
//console.log(Familia2);
//console.log(Familia2["Persona 1"]["Color"]);

//"Actividad"
var Familia = {
    "Persona 1": {
        "Nombre" : "David",
        "Apellidos": "Vite Bautista",
        "Gustos": ["Musica", "Peliculas"],
        "Color" : "Morado",
        "Saludo" : "Hola Mundo",
    },
    "Persona 2": {
        "Nombre" : "Rodrigo",
        "Apellidos": "Vite Bautista",
        "Gustos": ["Musica", "Peliculas"],
        "Color" : "Azul",
        "Saludo" : "Lolis",
    },
    "Persona 3": {
        "Nombre" : "Cristian",
        "Apellidos": "Vite Bautista",
        "Gustos": ["Musica", "Juegos"],
        "Color" : "Rojo",
        "Saludo" : ":v",
    },
    "Persona 4": {
        "Nombre" : "David",
        "Apellidos": "Vite Aquino",
        "Gustos": ["Musica", "Arrglar"],
        "Color" : "Rojo",
        "Saludo" : "Hola",
    },
    "Persona 5": {
        "Nombre" : "Margarita",
        "Apellidos": "Bautista Velasco",
        "Gustos": ["Coser", "Experimeentar En La Cocina"],
        "Color" : "Rosa",
        "Saludo" : "Hola",
    },

};